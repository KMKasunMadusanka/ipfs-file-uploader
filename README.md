# IPFS-file-uploader

A file uploading system for [IPFS] (https://ipfs.io/).


Follow bellow guidelines in order to run the application in Windows environment.

## Prerequisites

1. Download [IPFS-go] (https://dist.ipfs.io/#go-ipfs).
2. extract the folder and open terminal in that location, the type following commands.

```bash
ipfs init # initialize the IPFS. only one time run command.
ipfs config Addresses.Gateway /ip4/0.0.0.0/tcp/9001 # change the port number to 9001 of IPFS configuaration
ipfs daemon # run the IPFS server
```

## Commands

Open code from any IDE which is compatible with Spring Boot and do the following tasks


1. Run the project. # make sure your IPFS server is up and running
2. Open the browser window and go to the http://localhost:8080/. 
3. Then Upload the files and enjoy. :)

## For Linux Users

Go to the the UploadController.java class and change all .bat file extensions to .sh

eg :- `ProcessBuilder pb = new ProcessBuilder("src/shell/ipfs_genarate.bat");` into `ProcessBuilder pb = new ProcessBuilder("src/shell/ipfs_genarate.sh");`

## Bugs

### Windows 

Dynamic file upload function will not working. 

### Linux

When after upload the dynmic file (by ticking the dynamic file), 
IPFS server will automatically close. So make sure to restart the IPFS serve before proceed further.