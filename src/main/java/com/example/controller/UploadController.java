package com.example.controller;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.FileObj;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.apache.poi.ss.usermodel.CellType;

@Controller
public class UploadController {

	private final String ExcelFileName = "Ipfs_data.xlsx";

	private static final Logger logger = LogManager.getLogger(UploadController.class);
	private static String[] COLUMNs = { "File Name", "IPFS File Path" };

	String allIPFSPaths = "";
	String Path = null;
	File newFile;
	String line = null;

	ArrayList<FileObj> ExecelList = new ArrayList<>();

	public UploadController() {

		//initIPFS();
		logger.info("Application call to main class constuctor.");
		ExecelList.clear();
		logger.info("ExcelList Array list succesfully cleaned.");
		File excel = new File(ExcelFileName);
		logger.info(ExcelFileName + " created !");

		if (excel.exists()) {
			try {
				for (FileObj obj : readExcel()) {
					ExecelList.add(obj);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	// load the web page in the initial stage
	@GetMapping("/")
	public String listUploadedFiles(Model model) {

		logger.info("Home page successfully loaded!");
		model.addAttribute("ExecelList", ExecelList);
		logger.info("Load data to the ExcelList Array List");
		return "uploadForm";
	}

	// when click on the submit button this method will fire
	@PostMapping("/")
	public String handleFileUpload(@RequestParam("file") MultipartFile[] uploadingFiles,
			@RequestParam(value = "dynamicCheck", required = false) String checkboxValue, Model model) {

		if (isFileEmpty(uploadingFiles)) {

			ArrayList<FileObj> fileArray = new ArrayList<>();
			ArrayList<FileObj> filedetails = new ArrayList<>();

			for (MultipartFile file : uploadingFiles) {

				// create file in a local folder
				try {

					FileObj obj = new FileObj();

					obj.setName(file.getOriginalFilename());
					obj.setSize(readableFileSize(file.getSize()));
					filedetails.add(obj);

					newFile = this.convert(file);
					model.addAttribute("fileSizeArray", filedetails);

				} catch (IllegalStateException | IOException e1) {

					e1.printStackTrace();
				}

			}

			// execute the shell files and genarates the paths
			try {

				ProcessBuilder pb = new ProcessBuilder("src/shell/ipfs_genarate.sh");
				Process p = pb.start();
				InputStream is = p.getInputStream();
				StringBuffer sb = new StringBuffer();
				int j = 0;
				while ((j = is.read()) != -1)
					sb.append((char) j);

				String text = sb.toString();
				String lines[] = text.split("\\r?\\n");
				String hash = "";
				String combinedName = "";

				for (int r = 0; r < lines.length; r++) {
					line = lines[r];
					if (line.contains("added")) {
						if (line.contains("/")) {

							hash = line.split(" ")[1];

							if (checkboxValue != null) {
								combinedName = combinedName + line.split("/")[1] + ";\n";
							} else {

								System.out.println("not checked");
								Path = "https://ipfs.io/ipfs/" + hash;

								FileObj obj = new FileObj();

								obj.setName(line.split("/")[1]);
								obj.setPath(Path);

								fileArray.add(obj);

								System.out.println("file Name :- " + line.split("/")[1]);
								System.out.println("Ipfs Path:- " + Path);
							}

						} else {

							if (checkboxValue != null) {

								System.out.println("checked");

								hash = line.split(" ")[1];

								Path = "https://gateway.ipfs.io/ipns/" + geanrateIPNS(hash);

								FileObj obj = new FileObj();

								obj.setName(combinedName);
								obj.setPath(Path);

								fileArray.add(obj);							

								System.out.println("file Name :- " + combinedName);
								System.out.println("Ipfs Path:- " + Path);
							}

						}
					}
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

			model.addAttribute("message", null);
			model.addAttribute("fileArray", fileArray);
			// write to excel file
			try {

				ExecelList.clear();

				for (FileObj obj : fileArray) {
					ExecelList.add(obj);
				}

				File excel = new File(ExcelFileName);
				if (excel.exists()) {
					for (FileObj obj : readExcel()) {
						ExecelList.add(obj);
					}
				}

				writeToExcel(ExecelList);
				model.addAttribute("ExecelList", ExecelList);
				// delete data from folder after upload process finishes
				FileUtils.cleanDirectory(new File("IPFS_Files"));

			} catch (IOException e) {
				System.out.println("application need to read Ipfs_data.xlsx file before write to it.");
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			model.addAttribute("fileArray", null);
			model.addAttribute("message", "Please Select file to upload!");
		}

		return "uploadForm";
	}

	public void initIPFS() {

		ProcessBuilder pb = new ProcessBuilder("src/shell/ipfs_daemon.sh");
		Process p;
		try {
			p = pb.start();
			InputStream is = p.getInputStream();
			StringBuffer sb = new StringBuffer();
			int j = 0;
			while ((j = is.read()) != -1)
				sb.append((char) j);

			String text = sb.toString();

			System.out.println(text);

		} catch (IOException e) {
			logger.error(e);
		}

	}

	public String geanrateIPNS(String hash) {

		ProcessBuilder pb = new ProcessBuilder("src/shell/ipns.sh", hash);
		Process p;
		try {
			p = pb.start();
			InputStream is = p.getInputStream();
			StringBuffer sb = new StringBuffer();
			int j = 0;
			while ((j = is.read()) != -1)
				sb.append((char) j);

			String text = sb.toString();
			String lines[] = text.split("\\r?\\n");

			
			for (int r = 0; r < lines.length; r++) {
				line = lines[r];
				if (line.contains("Published")) {
					System.out.println(line.split(":")[0].split(" ")[2]);
					return line.split(":")[0].split(" ")[2];
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public boolean isFileEmpty(MultipartFile[] files) {

		if (files.length > 0) {
			return true;
		}

		return false;

	}

	// convert Multipart file to file type
	public File convert(MultipartFile file) throws IOException {
		File folder = new File("IPFS_Files");
		folder.mkdir();

		File convFile = new File("IPFS_Files/" + file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	public static String readableFileSize(long size) {
		if (size <= 0)
			return "0";
		final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
		int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
		return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}

	public void writeToExcel(ArrayList<FileObj> files) throws IOException {

		Workbook workbook = new XSSFWorkbook();

		Sheet sheet = workbook.createSheet("Ipfs_data");

		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setColor(IndexedColors.BLUE.getIndex());

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		// Row for Header
		Row headerRow = sheet.createRow(0);

		// Header
		for (int col = 0; col < COLUMNs.length; col++) {
			Cell cell = headerRow.createCell(col);
			cell.setCellValue(COLUMNs[col]);
			cell.setCellStyle(headerCellStyle);
		}

		int rowIdx = 1;
		for (FileObj file : files) {
			Row row = sheet.createRow(rowIdx++);

			row.createCell(0).setCellValue(file.getName());
			row.createCell(1).setCellValue(file.getPath());
		}

		FileOutputStream fileOut = new FileOutputStream(ExcelFileName);
		workbook.write(fileOut);
		fileOut.close();
		workbook.close();
	}

	public ArrayList<FileObj> readExcel() throws IOException {

		FileInputStream excelFile = new FileInputStream(new File(ExcelFileName));
		Workbook workbook = new XSSFWorkbook(excelFile);

		Sheet sheet = workbook.getSheet("Ipfs_data");
		Iterator<Row> rows = sheet.iterator();

		int rowItorator = 0;
		int colItorator = 0;
		ArrayList<FileObj> fileList = new ArrayList<>();

		while (rows.hasNext()) {
			Row currentRow = rows.next();
			Iterator<Cell> cellsInRow = currentRow.iterator();

			if (rowItorator != 0) {
				FileObj obj = new FileObj();
				while (cellsInRow.hasNext()) {
					Cell currentCell = cellsInRow.next();
					if (currentCell.getCellTypeEnum() == CellType.STRING) {
						if (colItorator == 0) {
							obj.setName(currentCell.getStringCellValue());
						}
						if (colItorator == 1) {
							obj.setPath(currentCell.getStringCellValue());
						}
					}
					colItorator = colItorator + 1;
				}
				fileList.add(obj);
			}
			colItorator = 0;
			System.out.println();
			rowItorator = rowItorator + 1;
		}

		rowItorator = 0;

		workbook.close();
		excelFile.close();
		return fileList;
	}

}
