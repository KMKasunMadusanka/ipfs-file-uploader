echo off

set /a c=1
setlocal ENABLEDELAYEDEXPANSION

FOR /F "tokens=5 delims= " %%I IN (
    'netstat -ano ^| find "127.0.0.1:5900" ^| find "CLOSE_WAIT"'
) DO (
    set /a c=c+1
    set /a last=%%I
)
if %c% geq 10 (
   taskkill /PID !last!
)

endlocal

ipfs name publish %1
ipfs daemon