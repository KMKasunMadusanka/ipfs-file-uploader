#!/bin/bash
kill $(lsof -t -i:9001)
ipfs init
ipfs config Addresses.Gateway /ip4/0.0.0.0/tcp/9001
ipfs daemon &
